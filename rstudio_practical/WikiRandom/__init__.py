import datetime
import logging
import sys

import azure.functions as func
import requests

WIKIPEDIA_RANDOM_URL = "https://en.wikipedia.org/wiki/Special:Random"


class InfoFilter(logging.Filter):
    def filter(self, rec):
        return rec.levelno in (logging.DEBUG, logging.INFO)


def init_logger():
    logger = logging.getLogger("__name__")
    logger.setLevel(logging.DEBUG)
    log_fmt = logging.Formatter("%(asctime)s | %(name)s | %(levelname)s : %(message)s")

    stdout_handler = logging.StreamHandler(sys.stdout)
    stdout_handler.setLevel(logging.DEBUG)
    stdout_handler.addFilter(InfoFilter())
    stdout_handler.setFormatter(log_fmt)
    stderr_handler = logging.StreamHandler(sys.stderr)
    stderr_handler.setLevel(logging.WARNING)
    stderr_handler.setFormatter(log_fmt)

    logger.addHandler(stdout_handler)
    logger.addHandler(stderr_handler)

    return logger


def main(mytimer: func.TimerRequest, outblob: func.Out[bytes]) -> None:
    logger = init_logger()

    utc_timestamp = (
        datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).isoformat()
    )

    if mytimer.past_due:
        logger.info("The timer is past due!")
    else:
        logger.debug("Timer not past due.")
    
    with requests.get(WIKIPEDIA_RANDOM_URL, allow_redirects=True) as r:
        if r.status_code != 200:
            err_msg = f"Request returned {r.status_code}, expected 200"
            logger.error(err_msg)
            outblob.set(err_msg.encode("utf-8"))
        else:
            logger.info(f"Retrieved {r.url}")
            outblob.set(r.content)

    logger.info("Python timer trigger function ran at %s", utc_timestamp)
