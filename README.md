# RStudio Practical

Practical assessment for RStudio.

## Prerequisites

The following packages/libraries must be installed to deploy this function app:
- Python 3.9
- [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)
- [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli)
- [Azure Function Core Tools 4.x](https://docs.microsoft.com/en-us/azure/azure-functions/functions-run-local?tabs=v4%2Clinux%2Ccsharp%2Cportal%2Cbash#v2)

## Deploying
In order to deploy the function app, you will need to have a subscription in Azure 
(cannot be a free trial). Once the subscription is setup, run `az login` to 
authenticate with Azure. This lets Terraform know where it's deploying.

The function app can be provisioned and deployed in Azure by running the following 
steps from within the `terraform-az` directory:
```bash
cd terraform-az
terraform init
terraform plan
# Check that the output looks correct
terraform apply
```
