terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
    }
  }
}

provider "azurerm" {
  features {
    resource_group {
      prevent_deletion_if_contains_resources = false
    }
  }
}

resource "azurerm_resource_group" "rg" {
  name     = "rg-${var.deployment_environment}-${var.project_name}"
  location = var.location
}

resource "azurerm_storage_account" "storage_account" {
  name                     = "stip${var.deployment_environment}${var.project_name}"
  resource_group_name      = azurerm_resource_group.rg.name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_storage_container" "blob_container" {
  name                  = var.project_name
  storage_account_name  = azurerm_storage_account.storage_account.name
  container_access_type = "blob"
}

resource "azurerm_application_insights" "app_insights" {
  name                = "appinsights-${var.deployment_environment}-${var.project_name}"
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  application_type    = "other"
}

resource "azurerm_service_plan" "asp" {
  name                = "asp-${var.deployment_environment}-${var.project_name}"
  resource_group_name = azurerm_resource_group.rg.name
  location            = var.location
  os_type             = "Linux"
  sku_name            = "B1"
}

resource "azurerm_linux_function_app" "funcapp" {
  name                = "func-${var.deployment_environment}-${var.project_name}"
  resource_group_name = azurerm_resource_group.rg.name
  location            = var.location

  service_plan_id = azurerm_service_plan.asp.id

  enabled    = true
  https_only = true

  storage_account_name       = azurerm_storage_account.storage_account.name
  storage_account_access_key = azurerm_storage_account.storage_account.primary_access_key

  app_settings = {
    "CONTAINER_NAME" = azurerm_storage_container.blob_container.name
  }

  connection_string {
    name  = "AZURE_STORAGE_CONNECTIONSTRING"
    type  = "Custom"
    value = azurerm_storage_account.storage_account.primary_blob_connection_string
  }

  site_config {
    application_insights_connection_string = azurerm_application_insights.app_insights.connection_string
    application_insights_key               = azurerm_application_insights.app_insights.instrumentation_key

    application_stack {
      python_version = 3.9
    }

    ip_restriction {
      name       = "DevIPAllowList"
      action     = "Allow"
      ip_address = var.allowed_ips
      priority   = 300
    }
  }
}

resource "time_sleep" "wait_for_funcapp_init" {
  depends_on = [
    azurerm_linux_function_app.funcapp
  ]
  create_duration = "30s"
}

locals {
  publish_code_command = "func azure functionapp publish ${azurerm_linux_function_app.funcapp.name}"
}

resource "null_resource" "funcapp_publish" {
  provisioner "local-exec" {
    working_dir = var.function_app_path
    command     = local.publish_code_command
  }
  depends_on = [
    azurerm_linux_function_app.funcapp,
    time_sleep.wait_for_funcapp_init,
    local.publish_code_command
  ]
  triggers = {
    publish_code_command = local.publish_code_command
  }
}
