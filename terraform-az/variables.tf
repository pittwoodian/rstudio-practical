variable "project_name" {
  type        = string
  description = "Project name used to generate resource names"
}

variable "location" {
  type        = string
  description = "Region to deploy to in Azure"
}

variable "deployment_environment" {
  type        = string
  description = "Environment being deployed to (dev, test, stage, prod, etc.)"
}

variable "allowed_ips" {
  type        = string
  description = "IP addresses allowed for access to the function app"
}

variable "function_app_path" {
  type        = string
  description = "Path to the function app to deploy"
}
